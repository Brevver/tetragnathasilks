# Tetragnatha Silk Evolution with a focus on the Hawaiian "Spiny-leg" Taxa
## Michael Brewer (brewermi14 \<at\> ecu \<dot\> edu; @Prof_Brewer on Twitter)
### Other analyses run by Cory Berger

:spider: :spider_web:

## 2020-04-20

## FUSTr runs
### FUSTr downloaded and installed on 2020-04-20 via documented steps
### SILEX installed via CONDA on 2020-04-20

```
git clone https://github.com/tijeco/FUSTr.git
#Installed

conda install -c tijeco silix #version 1.2.11
conda install snakemake #5.10.0
conda install iqtree #IQ-TREE multicore version 2.0-rc2 for Linux 64-bit built Mar 28 2020

FUSTr -d ./more_taxa -t 24
```

###Copy all final phylip codon alignments for future use and sharing

```
mkdir final_FUSTr_codon_alignments

find ./ -type f -name *.codon.phylip -exec cp {} final_FUSTr_codon_alignments \;
```

## DIAMOND searches for annotations
### Ptep genome protein FASTA file downloaded from GenBank on 2020-04-29 as __Ptep_proteins_NCBI_20200429.faa__. All Ptep silks downloaded from NCBI on 2020-04-20 as __Ptep_silk_PEP.fasta__.

### T. clavipes genome protein FASTA file downloaded from NCBI on 2020-05-01 as __Tclav_proteins_NCBI_20200501.fasta__.

### Raw PEP alignments from FUSTr used for annotations

```
mkdir familiy_PEP_fastas_raw
cp Families/*.fa family_PEP_fastas_raw/
tar -czvf family_PEP_fastas_raw.tar.gz family_PEP_fastas_raw

cat Ptep_proteins_NCBI_20200429.faa silk_DIAMOND_db/Ptep_silk_PEP.fasta > Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI.fasta
  
conda install -c bioconda diamond #version 0.914

mkdir DIAMOND_runs
cd DIAMOND_runs
```

#### Ptep DIAMOND searches

```
sed "s/ /_/g" Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI.fasta > Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES.fasta

diamond makedb --in Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES.fasta -d Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES_DIAMONDdb

for FileName in family_PEP_fastas_raw/*.fa; do diamond blastp -d Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES_DIAMONDdb -q $FileName -o $FileName"_DIAMOND_out_EVALUE1e-05.m8" -e 0.00001 --sensitive; done

for FileName in family_PEP_fastas_raw/*.m8; do base=`ls $FileName | cut -d"/" -f2 | cut -d"." -f1`; sort -g -k 11 $FileName | sed "s/^/$base-POOP/g" | sed "s/-POOP/\t/g" | sed "/^\s*$/d"; done > Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv

for FileName in family_PEP_fastas_raw/*.m8; do base=`ls $FileName | cut -d"/" -f2 | cut -d"." -f1`; tophit=`sort -g -k 11 $FileName | head -n 1`; echo $base"-POOP"$tophit | sed "s/-POOP/\t/g" | sed "/^\s*$/d" | sed "s/ /\t/g"; done > Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES_DIAMONDout_AllFamilies_TopHitAnnot_m8.csv
```

#### Tclav DIAMOND searches

```
sed "s/ /_/g" Tclav_proteins_NCBI_20200501.fasta > Tclav_proteins_NCBI_20200501_NOSPACES.fasta

diamond makedb --in Tclav_proteins_NCBI_20200501_NOSPACES.fasta -d Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDdb

for FileName in family_PEP_fastas_raw/*.fa; do diamond blastp -d Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDdb -q $FileName -o $FileName"_Tclav_DIAMOND_out_EVALUE1e-05.m8" -e 0.00001 --sensitive; done

for FileName in family_PEP_fastas_raw/*Tclav*.m8; do base=`ls $FileName | cut -d"/" -f2 | cut -d"." -f1`; sort -g -k 11 $FileName | sed "s/^/$base-POOP/g" | sed "s/-POOP/\t/g" | sed "/^\s*$/d"; done > Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv

for FileName in family_PEP_fastas_raw/*Tclav*.m8; do base=`ls $FileName | cut -d"/" -f2 | cut -d"." -f1`; tophit=`sort -g -k 11 $FileName | head -n 1`; echo $base"-POOP"$tophit | sed "s/-POOP/\t/g" | sed "/^\s*$/d" | sed "s/ /\t/g"; done > Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_TopHitAnnot_m8.csv
```

##### Recovering Silk Annotations from Tclav DIAMOND searches

```
grep -i 'silk\|spidroin\|glue\|aggregate\|flagelliform' Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv | cut -f1 | sort | uniq > Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv_SilkFamiliesFUSTr.txt

printf 'spidroin\naggregate\naggregate\nsilk gland factor\naggregate\nmajor+minor ampulates\naggregate\nflagelliform+ampulate\naciniform' > Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv_SilkFamiliesFUSTr_Annots.txt

paste Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv_SilkFamiliesFUSTr.txt Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv_SilkFamiliesFUSTr_Annots.txt > Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDout_AllFamilies_AllHitAnnot_m8.csv_SilkFamiliesFUSTr_Annots_FINAL.txt
```

### Get silk families (as defined via DIAMOND searches below) under pervasive selection

```
grep "18703\|19435\|21837\|24899\|30334\|5412\|54136\|68763\|9974" final_results/famsUnderSelection.txt
```

| FUSTr Family | Tclav Silk Annotation | Spiny-leg Taxa | No Web Taxa | Pervasive Selection |
| --- | --- | :---: | :---: | :---: |
| family_18703 | spidroin | Tbrevi Tperreirai2 Tquasi Twaika | Eroleonina | NA |
| family_19435 | aggregate | Tbrevi Tpolychromata Twaika | NA | NA |
| family_21837 | aggregate | Ttantalus | NA | NA |
| family_24899 | silk gland factor | Tperreirai2 Tquasi Twaika | NA | NA |
| family_30334 | aggregate | NA | NA | 3 residues |
| family_5412  | major+minor ampulates | Tbrevi Twaika | Australomimetus Malkaridaesp17 | NA |
| family_54136 | aggregate | NA | NA | NA |
| family_68763 | flagelliform+ampulate | NA | NA | 15 residues |
| family_9974  | aciniform | Tkauaiensis Tperreirai1 Tperreirai2 Ttantalus | NA | NA |

##### Collect and rename codon alignments for Tclav silk homologous FUSTr families

```
mkdir families_with_Tclav_silk_homologs

cp Families/family_18703_dir/family_18703.codon.phylip families_with_Tclav_silk_homologs/                                

cp Families/family_19435_dir/family_19435.codon.phylip families_with_Tclav_silk_homologs/

cp Families/family_21837_dir/family_21837.codon.phylip families_with_Tclav_silk_homologs/

cp Families/family_24899_dir/family_24899.codon.phylip families_with_Tclav_silk_homologs/                                

cp Families/family_30334_dir/family_30334.codon.phylip families_with_Tclav_silk_homologs/                                

cp Families/family_5412_dir/family_5412.codon.phylip families_with_Tclav_silk_homologs/                                

cp Families/family_54136_dir/family_54136.codon.phylip families_with_Tclav_silk_homologs/

cp Families/family_68763_dir/family_68763.codon.phylip families_with_Tclav_silk_homologs/                                

cp Families/family_9974_dir/family_9974.codon.phylip families_with_Tclav_silk_homologs/ 

for FileName in families_with_Tclav_silk_homologs/*.phylip; do python ~/FUSTr/utils/FusterID.py -d ./ -i $FileName > $FileName"_renamed.phylip"; done
```

##### Build trees using IQTREE for Tclav silk homologous FUSTr families

Used UNREST model (non-reversible) to infer root during tree searches

```
for FileName in families_with_Tclav_silk_homologs/*_renamed.phylip; do sed "s/\//-/g" $FileName > $FileName"_CLEANED"; done

for FileName in families_
with_Tclav_silk_homologs/*renamed.phylip_CLEANED; do ~/iqtree-2.0-rc2-Linux/bin/iqtree -s $FileName --alrt 1000 -b 1000 -m UNREST -bnni -redo; done
```

**Family_18703 - spidroin**
<img src="TclavSilkHomologTrees/family_18703.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_19435 - aggregate**
<img src="TclavSilkHomologTrees/family_19435.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_21837 - aggregate**
<img src="TclavSilkHomologTrees/family_21837.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_24899 - silk gland factor**
<img src="TclavSilkHomologTrees/family_24899.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_30334 - aggregate**
<img src="TclavSilkHomologTrees/family_30334.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_5412 - major+minor ampulates**
<img src="TclavSilkHomologTrees/family_5412.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_54136 - aggregate**
<img src="TclavSilkHomologTrees/family_54136.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_68763 - flagelliform+ampulate**
<img src="TclavSilkHomologTrees/family_68763.codon.phylip_renamed.phylip_CLEANED.treefile.png">

**Family_9974 - aciniform**
<img src="TclavSilkHomologTrees/family_9974.codon.phylip_renamed.phylip_CLEANED.treefile.png">


## COATS ortholog searches, species tree building, and branch tests for selection

### COATS ortholog searches
#### COATS installed via docker on 2020-04-20
```
docker pull michaelsbrewer/coats_test

docker run -v /run/media/brewerlab/BigRAID/GillespieLab/AdamsTranscriptomes/more_taxa:/home/user/coats/data -it coats_test /bin/bash 

bash coats_test.sh -m TVBF -n 36 -p 24

cp FINAL_ALIGNMENTS/*.fasta data/
```

### Species tree building using Astral

__Outside of COATS__

```
for FileName in *RBHs.fasta; do iqtree -s $FileName -m MFP -nt AUTO -pre $FileName-iqtree; done

cat *.treefile > AdamsTetragnatha_MoreTaxa_COATSGeneTrees.txt

java -jar ~/Astral/astral.5.7.3.jar -i AdamsTetragnatha_MoreTaxa_COATSGeneTrees.txt -o AdamsTetragnatha_MoreTaxa_COATSGeneTrees_ASTRAL.tre
```

<img src="AdamsTetragnatha_MoreTaxa_COATSGeneTrees_ASTRAL.tre.png">

__Back in COATS__

```
mv data/AdamsTetragnatha_MoreTaxa_COATSGeneTrees_ASTRAL.tre data/AdamsTetragnatha_MoreTaxa_COATSGeneTrees_ASTRAL.treefile
cp data/AdamsTetragnatha_MoreTaxa_COATSGeneTrees_ASTRAL.treefile data/NULL.tre
cp data/NULL.tre data/spinyanc.tre
cp data/NULL.tre data/noweb.tre
```

### Set up models by placing branches in foreground using vim
```
vim data/spinyanc
vim data/noweb	
```

### Run COATS tests for selection along branches of interest 
```
bash paml_scripts/PAML_multimodel.sh 12
```

### Annotate COATS loci via DIAMOND searches
```
for FileName in FINAL_ALIGNMENTS/*.fasta; do base=`ls $FileName | cut -d"/" -f2 | cut -d"_" -f1`; grep -h -A 1 "^>TVBF" $FileName | sed "s/--//g" | sed "/^\s*$/d" | sed "s/^>TVBF/>$base/g"; done > TVBF_COATS_seqs_4annotations.fasta

sed "s/-//g" TVBF_COATS_seqs_4annotations.fasta | sed "s/AM/AM-/g" | sed "s/TVBF/TVBF-/g" > TVBF_COATS_seqs_4annotations_NoGaps.fasta

diamond blastx -d ../FUSTr_run/Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDdb -q TVBF_COATS_seqs_4annotations_NoGaps.fasta -o TVBF_COATS_seqs_4annotations_Tclav_DIAMOND_out_EVALUE1e-05.m8 -e 0.00001 --sensitive

diamond blastx -d ../FUSTr_run/Tclav_proteins_NCBI_20200501_NOSPACES_DIAMONDdb -q TVBF_COATS_seqs_4annotations_NoGaps.fasta -o TVBF_COATS_seqs_4annotations_Tclav_DIAMOND_out_EVALUE1e-05_TopHit.m8 -e 0.00001 --sensitive -k 1

diamond blastx -d ../FUSTr_run/Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES_DIAMONDdb.dmnd -q TVBF_COATS_seqs_4annotations_NoGaps.fasta -o TVBF_COATS_seqs_4annotations_PtepWithSilks_DIAMOND_out_EVALUE1e-05.m8 -e 0.00005 --sensitive

diamond blastx -d ../FUSTr_run/Ptep_proteins_NCBI_20200429_Ptep_silks_NCBI_NOSPACES_DIAMONDdb.dmnd -q TVBF_COATS_seqs_4annotations_NoGaps.fasta -o TVBF_COATS_seqs_4annotations_PtepWithSilks_DIAMOND_out_EVALUE1e-05_TopHit.m8 -e 0.00005 --sensitive -k 1
```

#### Cheack COATS loci annotations for silks

```
grep -i 'silk\|spidroin\|glue\|aggregate\|flagelliform' TVBF_COATS_seqs_4annotations_PtepWithSilks_DIAMOND_out_EVALUE1e-05*m8 | cut -f1 | sort | uniq
```

**NO SILKS!!!**

| Loci                                           | Length | AU.p.value | AU.AcceptReject | NULL.lnL     | noweb        | spinyanc     | NULLAIC       | nowebAIC      | spinyancAIC   | NULLxnowebAIC    | NULLxspinyAIC    | spinyxnowebAIC     | Ptep TopHit                                                                                            | Tclav TopHit                                                                     |                                                                                           | 
|------------------------------------------------|--------|------------|-----------------|--------------|--------------|--------------|---------------|---------------|---------------|------------------|------------------|--------------------|--------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------| 
| AM-TVBF-comp56825_c0_seq1.p1_RBHs.fasta_codeml | 115    | 0.0676     | +               | -2754.012809 | -2749.812355 | -2754.012819 | -5364.025618  | -5353.62471   | -5362.025638  | 10.4009079999996 | 1.9999799999996  | 8.40092800000002   | XP_015920997.1_synaptosomal-associated_protein_29-like_[Parasteatoda_tepidariorum]                     | PRD23226.1_Synaptosomal-associated_protein_29_[Trichonephila_clavipes]           |                                                                                           | 
| AM-TVBF-comp20801_c0_seq1.p1_RBHs.fasta_codeml | 50     | 0.0566     | +               | -1096.347079 | -1092.268955 | -1096.347085 | -2048.694158  | -2038.53791   | -2046.69417   | 10.1562479999998 | 1.99998799999958 | 8.1562600000002    | XP_015907098.1_uncharacterized_protein_LOC107439095_[Parasteatoda_tepidariorum]                        | PRD34456.1_E3_ubiquitin-protein_ligase_MARCH1_[Trichonephila_clavipes]           |                                                                                           | 
| TVBF-comp54710_c0_seq1.p1_RBHs.fasta_codeml    | 162    | 0.0532     | +               | -3174.811828 | -3171.356572 | -3174.720016 | -6205.623656  | -6196.713144  | -6203.440032  | 8.91051199999947 | 2.18362399999933 | 6.72688800000014   | XP_021001087.1_proteasome_subunit_alpha_type-5-like_[Parasteatoda_tepidariorum]                        | PRD23817.1_Psma8_[Trichonephila_clavipes]                                        |                                                                                           | 
| AM-TVBF-comp59149_c2_seq1.p1_RBHs.fasta_codeml | 160    | 0.0783     | +               | -5569.701808 | -5566.274589 | -5569.365348 | -10995.403616 | -10986.549178 | -10992.730696 | 8.8544380000003  | 2.67291999999907 | 6.18151800000123   | XP_015918233.1_MKI67_FHA_domain-interacting_nucleolar_phosphoprotein-like_[Parasteatoda_tepidariorum]  | PRD31157.1_RNA-binding_protein_39_[Trichonephila_clavipes]                       |                                                                                           | 
| AM-TVBF-comp58242_c4_seq2.p1_RBHs.fasta_codeml | 203    | 0.134      | +               | -4127.695226 | -4125.276122 | -4125.770863 | -8111.390452  | -8104.552244  | -8105.541726  | 6.83820799999921 | 5.84872600000017 | 0.989481999999043  | XP_015907830.1_serine/threonine-protein_kinase_pim-3_[Parasteatoda_tepidariorum]                       | PRD20491.1_Serine/threonine-protein_kinase_pim-3_[Trichonephila_clavipes]        |                                                                                           | 
| AM-TVBF-comp55490_c0_seq1.p1_RBHs.fasta_codeml | 66     | 0.229      | +               | -1213.676188 | -1211.436912 | -1213.676167 | -2283.352376  | -2276.873824  | -2281.352334  | 6.47855200000004 | 2.00004199999967 | 4.47851000000037   | XP_015920762.1_rab_GDP_dissociation_inhibitor_beta-like_[Parasteatoda_tepidariorum]                    | PRD28495.1_Gdi1_[Trichonephila_clavipes]                                         |                                                                                           | 
| AM-TVBF-comp40348_c0_seq1.p1_RBHs.fasta_codeml | 88     | 0.119      | +               | -1997.652992 | -1995.566122 | -1997.652986 | -3851.305984  | -3845.132244  | -3849.305972  | 6.17374000000018 | 2.00001199999997 | 4.17372800000021   | XP_021003528.1_bystin-like_[Parasteatoda_tepidariorum]                                                 | PRD22191.1_Bystin_[Trichonephila_clavipes]                                       |                                                                                           | 
| AM-TVBF-comp56762_c0_seq1.p1_RBHs.fasta_codeml | 37     | 0.0924     | +               | -956.257937  | -954.595992  | -956.257935  | -1768.515874  | -1763.191984  | -1766.51587   | 5.32388999999989 | 2.00000399999999 | 3.3238859999999    | XP_015909333.1_N-alpha-acetyltransferase_35                                                            | _NatC_auxiliary_subunit_[Parasteatoda_tepidariorum]                              | PRD35759.1_actin-like_protein_[Trichonephila_clavipes]                                    | 
| AM-TVBF-comp40720_c0_seq1.p1_RBHs.fasta_codeml | 102    | 0.0609     | +               | -3723.285934 | -3721.78027  | -3720.998189 | -7302.571868  | -7297.56054   | -7295.996378  | 5.01132799999959 | 6.57549000000017 | -1.56416200000058  | XP_015927224.1_uncharacterized_protein_LOC107454521_[Parasteatoda_tepidariorum]                        | PRD28246.1_mRpL10_[Trichonephila_clavipes]                                       |                                                                                           | 
| AM-TVBF-comp58649_c1_seq1.p1_RBHs.fasta_codeml | 115    | 0.0728     | +               | -2242.455726 | -2241.337677 | -2242.455721 | -4340.911452  | -4336.675354  | -4338.911442  | 4.23609800000031 | 2.00001000000066 | 2.23608799999965   | XP_015914082.1_transitional_endoplasmic_reticulum_ATPase_isoform_X1_[Parasteatoda_tepidariorum]        | PRD24932.1_Transitional_endoplasmic_reticulum_ATPase_[Trichonephila_clavipes]    |                                                                                           | 
| AM-TVBF-comp49528_c2_seq1.p1_RBHs.fasta_codeml | 122    | 0.111      | +               | -2023.886871 | -2022.817667 | -2023.886886 | -3903.773742  | -3899.635334  | -3901.773772  | 4.1384079999998  | 1.99996999999985 | 2.13843799999995   | XP_015919257.1_NEDD8-conjugating_enzyme_Ubc12_[Parasteatoda_tepidariorum]                              | PRD36870.1_Ubc12_[Trichonephila_clavipes]                                        |                                                                                           | 
| TVBF-comp57571_c0_seq1.p1_RBHs.fasta_codeml    | 137    | 0.0572     | +               | -1660.0768   | -1659.210116 | -1660.07682  | -3176.1536    | -3172.420232  | -3174.15364   | 3.73336800000016 | 1.9999600000001  | 1.73340800000005   | XP_015929739.1_ras-related_protein_Rab-5C-like_[Parasteatoda_tepidariorum]                             | PRD34505.1_Ras-related_protein_Rab-5B_[Trichonephila_clavipes]                   |                                                                                           | 
| AM-TVBF-comp45396_c0_seq1.p1_RBHs.fasta_codeml | 240    | 0.129      | +               | -8653.186835 | -8652.495653 | -8653.164382 | -17162.37367  | -17158.991306 | -17160.328764 | 3.38236400000096 | 2.04490599999917 | 1.33745800000179   | XP_021002889.1_ATP_synthase_subunit_b                                                                  | _mitochondrial-like_[Parasteatoda_tepidariorum]                                  | PRD27893.1_Alas1_[Trichonephila_clavipes]                                                 | 
| TVBF-comp44613_c0_seq1.p1_RBHs.fasta_codeml    | 38     | 0.238      | +               | -943.853123  | -943.267975  | -943.853125  | -1743.706246  | -1740.53595   | -1741.70625   | 3.17029600000001 | 1.99999600000001 | 1.1703             | XP_015923952.1_ankyrin_repeat_domain-containing_protein_27-like_isoform_X1_[Parasteatoda_tepidariorum] | PRD21309.1_ddost_[Trichonephila_clavipes]                                        |                                                                                           | 
| AM-TVBF-comp56406_c0_seq1.p1_RBHs.fasta_codeml | 115    | 0.0752     | +               | -2332.876842 | -2332.360741 | -2332.873692 | -4521.753684  | -4518.721482  | -4519.747384  | 3.03220200000032 | 2.00630000000001 | 1.02590200000031   | XP_015930172.1_cilia-_and_flagella-associated_protein_20_[Parasteatoda_tepidariorum]                   | PRD18873.1_Cilia-_and_flagella-associated_protein_20_[Trichonephila_clavipes]    |                                                                                           | 
| AM-TVBF-comp38265_c0_seq1.p1_RBHs.fasta_codeml | 102    | 0.162      | +               | -1351.963685 | -1351.453858 | -1351.96369  | -2559.92737   | -2556.907716  | -2557.92738   | 3.01965399999972 | 1.9999899999998  | 1.01966399999992   | XP_015931023.1_ras-related_protein_Rap-2c_[Parasteatoda_tepidariorum]                                  | PRD34333.1_Rap2c_[Trichonephila_clavipes]                                        |                                                                                           | 
| TVBF-comp148043_c0_seq1.p1_RBHs.fasta_codeml   | 149    | 0.0803     | +               | -2200.728882 | -2200.322678 | -2200.67244  | -4257.457764  | -4254.645356  | -4255.34488   | 2.81240799999978 | 2.11288400000012 | 0.699523999999656  | XP_021001478.1_putative_ATP-dependent_RNA_helicase_me31b_[Parasteatoda_tepidariorum]                   | PRD29127.1_Ddx39b_[Trichonephila_clavipes]                                       |                                                                                           | 
| AM-TVBF-comp58985_c0_seq2.p1_RBHs.fasta_codeml | 206    | 0.058      | +               | -4304.61664  | -4304.249948 | -4304.616752 | -8465.23328   | -8462.499896  | -8463.233504  | 2.73338400000102 | 1.99977600000057 | 0.733608000000459  | XP_015905272.1_ATP_synthase_subunit_beta                                                               | _mitochondrial_[Parasteatoda_tepidariorum]                                       | PRD30670.1_atp5b_[Trichonephila_clavipes]                                                 | 
| TVBF-comp49143_c0_seq2.p1_RBHs.fasta_codeml    | 131    | 0.099      | +               | -3303.95264  | -3303.670641 | -3302.197457 | -6463.90528   | -6461.341282  | -6458.394914  | 2.56399799999963 | 5.51036599999952 | -2.94636799999989  | XP_015911908.1_NADH_dehydrogenase_[ubiquinone]_iron-sulfur_protein_3                                   | _mitochondrial_[Parasteatoda_tepidariorum]                                       | PRD33939.1_NADH_dehydrogenase_[ubiquinone]_iron-sulfur_protein_3_[Trichonephila_clavipes] | 
| TVBF-comp57965_c0_seq1.p1_RBHs.fasta_codeml    | 344    | 0.0699     | +               | -8240.430426 | -8240.183618 | -8239.964296 | -16336.860852 | -16334.367236 | -16333.928592 | 2.49361600000339 | 2.93226000000141 | -0.438643999998021 | XP_015931017.1_elongation_factor_2_[Parasteatoda_tepidariorum]                                         | PRD26562.1_U5_small_nuclear_ribonucleoprotein_component_[Trichonephila_clavipes] |                                                                                           | 
| AM-TVBF-comp57459_c0_seq1.p1_RBHs.fasta_codeml | 264    | 0.0787     | +               | -5436.805873 | -5436.58816  | -5435.888615 | -10729.611746 | -10727.17632  | -10725.77723  | 2.43542600000001 | 3.8345160000008  | -1.3990900000008   | XP_015928180.1_60S_acidic_ribosomal_protein_P0-like_[Parasteatoda_tepidariorum]                        | PRD18687.1_RpLP0_[Trichonephila_clavipes]                                        |                                                                                           | 
| AM-TVBF-comp26055_c0_seq1.p1_RBHs.fasta_codeml | 87     | 0.0505     | +               | -1775.893417 | -1775.771478 | -1775.893415 | -3407.786834  | -3405.542956  | -3405.78683   | 2.24387800000022 | 2.00000399999999 | 0.243874000000233  | XP_015919407.1_obg-like_ATPase_1_[Parasteatoda_tepidariorum]                                           | PRD18689.1_ychF_[Trichonephila_clavipes]                                         |                                                                                           | 
| TVBF-comp58735_c0_seq1.p1_RBHs.fasta_codeml    | 196    | 0.0881     | +               | -5461.528485 | -5461.434055 | -5461.137077 | -10779.05697  | -10776.86811  | -10776.274154 | 2.1888600000002  | 2.782815999999   | -0.593955999998798 | XP_015927860.1_dnaJ_homolog_subfamily_A_member_1-like_[Parasteatoda_tepidariorum]                      | PRD25840.1_Dnajb4_[Trichonephila_clavipes]                                       |                                                                                           | 
| AM-TVBF-comp49097_c0_seq2.p1_RBHs.fasta_codeml | 111    | 0.0597     | +               | -2397.410284 | -2397.361848 | -2397.410301 | -4650.820568  | -4648.723696  | -4648.820602  | 2.09687200000008 | 1.99996600000031 | 0.096905999999763  | XP_015908426.1_V-type_proton_ATPase_subunit_E-like_[Parasteatoda_tepidariorum]                         | PRD23772.1_V-type_proton_ATPase_subunit_E_[Trichonephila_clavipes]               |                                                                                           | 
| AM-TVBF-comp54631_c0_seq1.p1_RBHs.fasta_codeml | 72     | 0.0669     | +               | -1167.288624 | -1167.263377 | -1165.056657 | -2190.577248  | -2188.526754  | -2184.113314  | 2.05049400000007 | 6.46393399999988 | -4.41343999999981  | XP_015928317.2_ubiquitin-conjugating_enzyme_E2_Q2-like_[Parasteatoda_tepidariorum]                     | PRD32062.1_Peptidyl-prolyl_cis-trans_isomerase_FKBP7_[Trichonephila_clavipes]    |                                                                                           | 
| AM-TVBF-comp59471_c5_seq1.p1_RBHs.fasta_codeml | 128    | 0.0502     | +               | -2186.27064  | -2186.255692 | -2186.194451 | -4228.54128   | -4226.511384  | -4226.388902  | 2.02989600000001 | 2.15237800000068 | -0.122482000000673 | XP_015919834.1_splicing_factor_U2AF_50_kDa_subunit_[Parasteatoda_tepidariorum]                         | PRD29982.1_Glutamine_synthetase_[Trichonephila_clavipes]                         |                                                                                           | 
| AM-TVBF-comp55223_c0_seq1.p1_RBHs.fasta_codeml | 115    | 0.164      | +               | -2159.003208 | -2158.988435 | -2158.880978 | -4174.006416  | -4171.97687   | -4171.761956  | 2.02954599999975 | 2.24445999999989 | -0.214914000000135 | XP_021003960.1_WD_repeat-containing_protein_44-like_[Parasteatoda_tepidariorum]                        | PRD24893.1_Chromatin_assembly_factor_1_subunit_B_[Trichonephila_clavipes]        |                                                                                           | 
| TVBF-comp49479_c0_seq1.p1_RBHs.fasta_codeml    | 158    | 0.0787     | +               | -4455.190984 | -4455.17856  | -4454.987072 | -8766.381968  | -8764.35712   | -8763.974144  | 2.02484799999911 | 2.40782399999989 | -0.382976000000781 | XP_015910542.1_synaptobrevin_homolog_YKT6_[Parasteatoda_tepidariorum]                                  | PRD32891.1_Synaptobrevin-like_protein_YKT6_[Trichonephila_clavipes]              |                                                                                           | 
| AM-TVBF-comp53073_c0_seq1.p1_RBHs.fasta_codeml | 101    | 0.0635     | +               | -2227.17115  | -2227.158844 | -2227.171143 | -4310.3423    | -4308.317688  | -4308.342286  | 2.02461200000016 | 2.00001400000019 | 0.024597999999969  | XP_015913297.1_electron_transfer_flavoprotein_subunit_beta-like_[Parasteatoda_tepidariorum]            | PRD33998.1_etfA_[Trichonephila_clavipes]                                         |                                                                                           | 
| AM-TVBF-comp36800_c0_seq1.p1_RBHs.fasta_codeml | 138    | 0.101      | +               | -4629.300256 | -4629.289924 | -4629.300301 | -9114.600512  | -9112.579848  | -9112.600602  | 2.02066400000149 | 1.99991000000045 | 0.020754000001034  | XP_015916180.1_peroxisomal_biogenesis_factor_19-like_[Parasteatoda_tepidariorum]                       | PRD20033.1_Peroxisomal_biogenesis_factor_19_[Trichonephila_clavipes]             |                                                                                           | 
| AM-TVBF-comp57815_c1_seq1.p1_RBHs.fasta_codeml | 94     | 0.128      | +               | -1860.620286 | -1860.616122 | -1860.620285 | -3577.240572  | -3575.232244  | -3575.24057   | 2.00832800000035 | 2.00000200000022 | 0.008326000000125  | XP_015926175.1_E3_ubiquitin-protein_ligase_synoviolin_[Parasteatoda_tepidariorum]                      | PRD31207.1_Trc8_[Trichonephila_clavipes]                                         |                                                                                           | 
| AM-TVBF-comp54231_c0_seq1.p1_RBHs.fasta_codeml | 162    | 0.088      | +               | -5953.859492 | -5953.856176 | -5953.069122 | -11763.718984 | -11761.712352 | -11760.138244 | 2.00663199999872 | 3.58073999999942 | -1.57410800000071  | XP_015919231.1_TATA-box-binding_protein-like_[Parasteatoda_tepidariorum]                               | PRD39237.1_TATA-box-binding_protein_[Trichonephila_clavipes]                     |                                                                                           | 
| AM-TVBF-comp53850_c1_seq1.p1_RBHs.fasta_codeml | 91     | 0.0558     | +               | -1841.162593 | -1841.162089 | -1841.162628 | -3538.325186  | -3536.324178  | -3536.325256  | 2.00100800000018 | 1.99992999999995 | 0.001078000000234  | XP_015920690.1_PRKCA-binding_protein-like_isoform_X1_[Parasteatoda_tepidariorum]                       | PRD32679.1_Vha36-1_[Trichonephila_clavipes]                                      |                                                                                           | 
| AM-TVBF-comp57563_c0_seq1.p1_RBHs.fasta_codeml | 93     | 0.0564     | +               | -1684.35199  | -1684.35199  | -1684.35199  | -3224.70398   | -3222.70398   | -3222.70398   | 2                | 2                | 0                  | XP_015910060.1_26S_proteasome_non-ATPase_regulatory_subunit_14_[Parasteatoda_tepidariorum]             | PRD19349.1_Rpn11_[Trichonephila_clavipes]                                        |                                                                                           | 


